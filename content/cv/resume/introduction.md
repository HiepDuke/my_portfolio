+++
title = "My Resume"
description = "AdiDoks is a Zola theme helping you build modern documentation websites, which is a port of the Hugo theme Doks for Zola."
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "cv/page.html"

[extra]
lead = ''
toc = true
top = false
+++

## Education
**Duke University**<br>
 **Master of Engineering in Electrical Engineering** <br>-
 **Data Analytics & Machine Learning Track** \
 *August 2023 - May 2025*<br>- GPA 3.85/4.0 <br>- Vingroup Science and Technology Full-ride Scholarship<br>- Teaching Assistant for ECE 270: Fields and Waves 

 **Nagoya University** \
  **Bachelor of Engineering in Electrical Engineering**\
  *October 2019 - Septemter 2023*<br>- GPA 4.15/4.3 <br>- MEXT Government Scholarship<br>

## Skills

**Languages:** C/C++, Python, Java, Rust, HTML/CSS, JavaScript, SQL, LaTeX, MATLAB \
**Frameworks:** Pytorch, Scikit-learn, Matplotlib, Seaborn, Flask, Django \
**Tools:** GIT, Emacs, AWS, REST API, Docker, Office, Confluence 

Cloud Computing, Algorithms and Data Structures, Machine Learning, Artificial Intelligence, Big Data, Software Development, Database Management Systems, Data Visualization, System Design, Linear Algebra, Web Development 
## Work Experiences

**Duke University** \
 **Software Engineer Intern** \
 *June 2024 - August 2024* 
  - Developed a scalable automated grading system, deployed on Kubernetes for production 
  - Integrated Docker Volume to enhance data security and seamless access from Kubernetes orchestration of Docker containers, ensuring high availability and efficient resource utilization
  - Collaborated closely with teammates working to build a core structure and an interactive webapp on Spring Boot for multi-role users resulting in a 60% increase in overall system maintainability 
  - Leveraged a robust CI/CD pipeline to automate build, test, and deployment processes, reducing deployment time by 30% and improving code reliability.
  
**Daimler** \
 **Software Engineer Intern** \
 *October 2022 - April 2023* 
 - Developed a full-stack Logistics Simulation Software utilizing Object-Oriented Programming in Python to optimize task sequences 
 <br>- Implemented algorithms adapting complex logistics regulations to reduce operation costs up to 50%
 <br>- Built an interactive dashboard to visualize material flows visualization and provide navigation instructions 
 <br>- Produced comprehensive software documentations for senior management for informed decision-making 
- Utilized ML models and data visualization to conduct comprehensive business analysis for a Green Tech energy project valued at $2 million, producing 168 MWh of electricity annually 
- Coordinated projects with other teams to ensure seamless integration and implementation of the products


**Mobility System - Nagoya University** \
 **Undergraduate Researcher** \
 *April 2022 - July 2023* 
 - Implemented a computationally intensive control technique for Autonomous Vehicles to low-powered devices 
 - Developed a simulation software on embedded system to analyze control feasibility based on hardware limitations
 -	Implemented Model Predictive Control (MPC) on Teensy4.0 (Arduino platform) and integrated into 1-10th scale vehicles with 95% control accuracy

**FPT Software** \
 **AI Software Engineer Intern** \
 *March 2022 - April 2022* 
- Automated Tax Form extraction using Optical Character Recognition (OCR) models and customized Natural Language Processing (NLP) techniques
- Developed AI software solutions for interpreting food expiration dates in various formats and languages 


## Technical Projects
**Attendance Taking System**  | SpringBoot, JavaFX, PostgreSQL<br>
Developed full-stack web and desktop applications using Spring Boot, JavaFX to automate attendance taking
-	Designed a QR Code-based system that generates a time-limited randomized link to verify credentials and location data, effectively preventing cheating and efficiently recording hundreds of users under 30 seconds
- Developed a user-friendly GUI web-app allowing multiple-role, multi-user access, and an intuitive JavaFX application for administration that effectively manages the PostgreSQL database
- Built an automated build and testing system with Docker and CI/CD in Gitlab, achieving 80% test coverage, streamlining the workflow for a team 5 developers

[**Adversarial Patch Attack**](../../../projects/adversarial-patch-attack/) | Machine Learning security, Pytorch, Adversarial Attack<br> 
Developed adversarially generated patches to fool image classifiers into misclassifying the sample
- Executed an untargeted adversarial attack using a translation and rotation robust 8x8 patch, significantly reducing ResNet18's test accuracy from 83.14% to 39.49%
- Fine-tuned ResNet50, VGG19, and DenseNet121 models to assess attack transferrability


[**Deep Learning Hyperparameter Tuning Interface**](../../../projects/hello-world/) | Web Development, Neural Network Model traning, Multi-Threading<br> 
Built a web-based interface for tuning DL models, tracking training progress, and visualizing results
- Designed an interactive UI allowing hyperparameter tuning and insightful visualizations with simple clicks
- Implemented multi-threading to seamlessly add new trials and enable to close/reopen the browser without affecting ongoing training process

[**Cardiovascular Disease Risk Prediction**](../../../projects/markdown-syntax/) | Data analysis, Matplotlib, Seaborn<br> 
Developed a predictive model for cardiovascular disease on a dataset of 208,854 patient records
- Conducted exploratory data analysis using Matplotlib and Seaborn to identify key patterns and insights in the data 
- Utilized Logistic Regression, Decision Tree based algorithms, neural networks, etc. achieving the most effective model of 83% AUC with Logistic Regression and 81% AUC with Naive Bayes for most scalable model

[**Airbnb listings in LA price predictions**](../../../projects/placeholder-text/) | Scikit, Matplotlib, Seaborn<br>
Developed Machine Learning models to predict hotels', homestays' prices in LA using scrapped data from Airbnb
- Implemented customized feature engineering, including target encoding for categorical features, custom missing data handling, and feature selection via base model performance
- Constructed stacked ensemble ML models, performed hyperparameter tuning, employed resampling techniques, and weighted impbalanced classes, resulting in 15% accuracy performance

## Contact


- Email: [hiep.nguyen(at)duke.edu](mailto:hiep.nguyen@duke.edu)
