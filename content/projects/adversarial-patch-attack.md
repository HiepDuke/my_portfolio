+++
title = "Adversarial Patch Attack"
description = "Most research on adversarial attacks focuses on creating imperceptible perturbations in images, but in many cases, adversaries will be willing to sacrifice undetectability if creating more noticeable attacks carries greater benefits."
date = 2024-04-28T09:19:42+00:00
updated = 2024-04-28T09:19:42+00:00
draft = false
template = "projects/page.html"

[taxonomies]
authors = ["Hiep Nguyen"]

[extra]
lead = "Most research on adversarial attacks focuses on creating imperceptible perturbations in images, but in many cases, adversaries will be willing to sacrifice undetectability if creating more noticeable attacks carries greater benefits."
+++




We carried out a white box attack against ResNet18 that involved creating a square-shaped adversarial patch. Applying this patch to a CIFAR-10 image causes ResNet18 to misclassify the sample with high probability.
    

## Abstract 
This paper is a replication and extension of the work by Brown et al. [1](https://arxiv.org/abs/1712.09665) on adversarial patches. We execute targeted and untargeted adversarial patch attacks with white box access to ResNet18; transfer those patches to ResNet50, VGG19, and DenseNet121; and investigate the attack success rate for patches trained to be robust to three families of transformations. Our main findings are (1) larger patches have a higher targeted ASR, (2) patches that are robust only to translations tend to be much more effective than patches that are robust to other types of transformations (robustness to rotations appears to cause a significant dip in ASR), and (3) for the CIFAR-10 dataset, target class has an extremely significant impact on ASR. 


[Click here](https://github.com/hiepnh14/Final-Project-ECE661) to the repository.
## Poster

[pdf](https://github.com/hiepnh14/Final-Project-ECE661/blob/main/ECE661%20Final%20Poster.pdf)
![Image 1](/ECE661%20Final%20Poster.png)
