+++
title = "AirBNB listings in LA price predictions"
description = "Lorem Ipsum Dolor Si Amet"
date = 2023-12-12T09:19:42+00:00
updated = 2023-12-12T09:19:42+00:00
draft = false
template = "projects/page.html"

[taxonomies]
authors = ["Hiep Nguyen"]

[extra]
lead = "Developed Machine Learning models to predict hotels’, homestays’ prices in LA using scrapped data from Airbnb"
+++


#### Dataset Introduction

Airbnb is an online platform that enables people to list, discover, and book accommodations worldwide. Founded in 2008, Airbnb has revolutionized the hospitality and lodging
industry by allowing individuals to rent out their properties, homes, apartments, or spare
rooms to travelers seeking short-term accommodations. Key points about Airbnb:
* **Accommodation Options**: Users can find various types of accommodations, including entire homes, apartments, villas, rooms in homes, treehouses, castles, and more.
* **Hosts and Guests**: Hosts can list their properties on the platform, set pricing, house
rules, and interact with potential guests. Travelers (guests) can search for accommodations based on location, price, amenities, and reviews.
* **Booking and Payments**: The platform facilitates bookings, handles payments securely, and provides a review system for hosts and guests to rate each other after their
stay.
* **Diverse Experiences**: Apart from lodging, Airbnb offers experiences hosted by locals
in various destinations, enabling travelers to explore unique activities, tours, workshops,
and cultural immersions.
* **Community and Trust**: Airbnb emphasizes community building and trust through
verified profiles, reviews, and secure payment systems to foster a sense of safety and
reliability among users.

#### Feature Engineering:
Some results of feature engineering:
![frequency](/frequency.png)

![heatmap](/heatmap.png)

## Machine Learning result
This is the confusion matrix with XGBoost
![xgb](/confusion_xgb.png)

#### Predictive Accuracies across different models:

![result](/resultcompare.png)

<!-- | Model | Accuracy | 
|-------|----------|
| Model 1 | 0.85 |
| Model 2 | 0.92 |
| Model 3 | 0.78 |
| Model 4 | 0.95 |
| Model 5 | 0.87 | -->
