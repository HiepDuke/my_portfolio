+++
title = "Cardiovascular Disease Prediction"
description = ""
date = 2024-01-27T09:19:42+00:00
updated = 2024-01-27T09:19:42+00:00
draft = false
template = "projects/page.html"

[taxonomies]
authors = ["Hiep Nguyen"]

[extra]
lead = "Developed a predictive model for cardiovascular disease on a dataset of 308,854 patient records"
+++

[Click here to the Jupyter Notebook](https://colab.research.google.com/drive/13AGTNHQ9YDAXwMyeEP0XM8lsyZ4EvXPs?usp=sharing)

## Introduction

Cardiovascular disease (CVD) remains a leading cause of morbidity and mortality globally, presenting a significant public health challenge. Analyzing cardiovascular prediction is crucial for several reasons: Early Detection, Personalized Medicine, Resource Allocation, Public Health Planning R&D.

In this study, we aimed to identify important factors that can help predict heart disease in people with different health conditions. We used a dataset from the CDC (Centers for Disease Control and Prevention) called `CVD_cleaned` which contains information about 308,854 patients, including their diet, doctor visit frequency, and exercise habits.

The visualizations and explanations in our report detail the connections between these factors and the risk of heart disease. We hope this information is helpful and welcome any questions you may have.

## EDA, Modelling 
Please refer to the [Jupyter Notebook](https://colab.research.google.com/drive/13AGTNHQ9YDAXwMyeEP0XM8lsyZ4EvXPs?usp=sharing)

## Conclusion

### Summary of Key Findings:

- The risk factors for cardiovascular disease are more intricate than initially thought. People with 'Good' general health status and the highest incidence of heart disease illustrate this complexity.
- The dataset was biased towards negative heart disease classifications. By applying SMOTE and resampling to balance the positive and negative cases, we achieved a more accurate representation in ROC/AUC analysis.
- We selected the Naïve Bayes classifier as our final model due to its simplicity and interpretability. The model's decision-making process, based on the input features, is easier to understand.

### Future Considerations for Improvement:
- Incorporating additional datasets to enhance the model's complexity.
- Exploring further with neural networks, particularly experimenting with different activation functions and optimizers to see if they improve model performance on test data.
- Leveraging Amazon AWS buckets more effectively to distribute data across clusters, which could accelerate the training of machine learning models.