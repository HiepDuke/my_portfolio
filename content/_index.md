+++
title = "Hiep's Portfolio"


# The homepage contents
[extra]
lead = 'Aspiring Machine Learning/Software Engineer!'
url = "/projects/"
url_button = "See my projects"
repo_version = "GitHub v0.1.0"
repo_license = "Open-source MIT License."
repo_url = "https://github.com/aaranxu/adidoks"

# Menu items
[[extra.menu.main]]
name = "CV"
section = "cv"
url = "/cv/resume/introduction/"
weight = 10

[[extra.menu.main]]
name = "Projects"
section = "projects"
url = "/projects/"
weight = 20

[[extra.list]]
title = "Problem Solver"
content = "As an engineering student, I enjoy solving problems, especially ones that are challenging and bringing benefits to people."
[[extra.list]]


title = "Competent Programmer"
content = "I am competent in Java, Python, C/C++ backed by hands-on experience gained through internship, project experiences, and graduate-level coursework."


[[extra.list]]
title = "Team Player"
content = 'Working in a team, I actively collaborate with team members, contribute valuable insights, and foster a positive and cooperative working environment.'

[[extra.list]]
title = "Soccer Player"
content = 'Striker/Winger are my favorite positions. I also play as middle defender or center back when the team needs.'


[[extra.list]]
title = "Sociable Adventurer"
content = "In my free time, I love hanging with my family and my friends. I enjoy hiking, eating out, travelling, and playing boardgame with them."



+++
