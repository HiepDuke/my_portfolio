Link to the deployed website: https://hiepnguyenn.netlify.app/

# Install Adidoks by Zola and deploy on Netlify
Here's how to install Adidoks by Zola and build and deploy it on Netlify:

Adidoks is a static site generator powered by Zola. This guide will walk you through the process of installing Adidoks, building your site, and deploying it on Netlify.

## Installation

1. Install Zola by following the instructions in the [Zola documentation](https://www.getzola.org/documentation/getting-started/installation/).

2. Clone the Adidoks repository:
  ```bash
   git clone https://github.com/cobidev/adidoks.git
  ```
3. Change into the adidoks directory:

```bash
cd adidoks
```
4. Building Your Site
Build your site using Zola:

```bash
zola build
```
Your site will be generated in the public directory.
5. Build, deploy on Gitlab:
- Modify the base_url in `config.toml`
- Change or create the file `.gitlab-ci.yml` if needed
- Build, configure pipelines in Gitlab (follow the instruction there)

6. Deploying on Netlify
- Sign up for a free Netlify account at Netlify.

- Log in to your Netlify account and click on "New site from Git."

- Connect your GitLab repository where your Adidoks site is stored.

7. Follow the default instruction in the Netlify website

- Netlify will build and deploy your site automatically.
- You can change the url of your website on General/Site information
8. Updating Your Site
- Make changes to your site as needed.

9. Rebuild your site using Zola:

```bash
zola build
```
10. Push the changes to your GitHub repository.

- Netlify will automatically rebuild and deploy your site with the latest changes.

That's it! Your Adidoks site is now built and deployed on Netlify.


# This is my portfolio webpage
### Hi there 👋 I'm Hiep

<!--
**hiepnh14/hiepnh14** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:


-->
<!-- Hi ![](https://user-images.githubusercontent.com/18350557/176309783-0785949b-9127-417c-8b55-ab5a4333674e.gif)My name is Hiep
============================================================================================================================ -->
- 🔭 I’m currently working on implementing MPC calculation on microcomputer and PoC on 1-10th Autonomous Vehicle.
- 🌱 I’m currently learning ML, Pytorch
- 📫 How to reach me: [hiepnguyen0014@gmail.com](mailto:hiepnguyen0014@gmail.com)
- 😄 Pronouns: he/him
- ⚡ Fun fact: I love sports and playing with animals (dogs, cats).
<!-- * ✉️  You can contact me at [hiepnguyen0014@gmail.com](mailto:hiepnguyen0014@gmail.com)
* 🚀  I'm currently working on [implementing MPC calculation on microcomputer and PoC on 1-10th Autonomous Vehicle.](http://d)
* 🧠  I'm learning Gin Web Framework, AI, Pytorch.
* ⚡  I love animals (dogs, cats) and have a strong interest in the environment.
 -->
### Skills


<p align="left">
<a href="https://www.python.org/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/python-colored.svg" width="36" height="36" alt="Python" /></a>
<a href="https://docs.microsoft.com/en-us/cpp/?view=msvc-170" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/c-colored.svg" width="36" height="36" alt="C" /></a>
<a href="https://docs.microsoft.com/en-us/cpp/?view=msvc-170" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/cplusplus-colored.svg" width="36" height="36" alt="C++" /></a>
<a href="https://go.dev/doc/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/go-colored.svg" width="36" height="36" alt="Go" /></a>
<a href="https://git-scm.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/git-colored.svg" width="36" height="36" alt="Git" /></a>
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/javascript-colored.svg" width="36" height="36" alt="JavaScript" /></a>
<a href="https://developer.mozilla.org/en-US/docs/Glossary/HTML5" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/html5-colored.svg" width="36" height="36" alt="HTML5" /></a>
 <a href="https://www.arduino.cc/" target="_blank" rel="noreferrer"> <img src="https://cdn.worldvectorlogo.com/logos/arduino-1.svg" alt="arduino" width="36" height="36"/> </a> 
  <a href="https://www.linux.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="36" height="36"/>
<a href="https://www.mysql.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/mysql-colored.svg" width="36" height="36" alt="MySQL" /></a>
<a href="https://flask.palletsprojects.com/en/2.0.x/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/flask-colored.svg" width="36" height="36" alt="Flask" /></a>
<a href="https://www.djangoproject.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/django-colored.svg" width="36" height="36" alt="Django" /></a>
</p>


### Socials

<p align="left"> <a href="http://www.instagram.com/hiep.nguyen14/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/instagram.svg" width="32" height="32" /></a> <a href="https://www.linkedin.com/in/hoang-hiep-nguyen/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/linkedin.svg" width="32" height="32" /></a></p>

# This website is powered by Zola Theme AdiDoks

AdiDoks is a modern documentation theme, which is a port of the Hugo
theme [Doks](https://github.com/h-enk/doks) for Zola.



## License

**AdiDoks** is distributed under the terms of the
[MIT license](https://github.com/aaranxu/adidoks/blob/main/LICENSE).
